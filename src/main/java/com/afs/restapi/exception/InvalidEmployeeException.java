package com.afs.restapi.exception;

public class InvalidEmployeeException extends RuntimeException {

    public InvalidEmployeeException() {
        super("Invalid employee");
    }
}
