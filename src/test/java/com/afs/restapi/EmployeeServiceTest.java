package com.afs.restapi;

import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

    @SpyBean
    EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Test
    void should_throw_invalid_employee_exception_when_create_given_employee_age_less_than_18() {
        Employee employee = new Employee("John Smith", 17, "Male", 5000);

        assertThrows(InvalidEmployeeException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_throw_invalid_employee_exception_when_create_given_employee_age_larger_than_65() {
        Employee employee = new Employee("John Smith", 66, "Male", 5000);

        assertThrows(InvalidEmployeeException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        Employee employee = new Employee("John Smith", 32, "Male", 5000);

        employeeService.create(employee);

        verify(employeeRepository, times(1)).insert(employee);
    }

    @Test
    void should_return_all_employees_when_findAll() {
        employeeService.findAll();

        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_return_employee_by_id_when_findById_given_id() {
        Long id = 1L;

        employeeService.findById(id);

        verify(employeeRepository, times(1)).findById(id);
    }

    @Test
    void should_return_employee_by_gender_when_findByGender_given_gender() {
        String gender = "Male";

        employeeService.findByGender(gender);

        verify(employeeRepository, times(1)).findByGender(gender);
    }

    @Test
    void should_return_employee_by_page_when_findByPage_given_page_number_and_page_size() {
        Integer pageNumber = 1;
        Integer pageSize = 10;

        employeeService.findByPage(pageNumber, pageSize);

        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_throw_InactiveEmployeeException_when_update_given_inactive_employee() {
        Long id = 1L;
        Employee employee = new Employee("John Smith", 32, "Male", 5000);
        employee.setActiveStatus(false);

        doReturn(employee).when(employeeRepository).findById(id);

        assertThrows(InactiveEmployeeException.class, () -> employeeService.update(id, employee));
    }

    @Test
    void should_update_employee_when_update_given_active_employee() {
        Long id = 1L;
        Employee employee = new Employee("John Smith", 32, "Male", 5000);
        Employee employeeToUpdate = new Employee("John Smith", 33, "Male", 5001);

        doReturn(employeeToUpdate).when(employeeRepository).findById(id);

        employeeService.update(id, employee);

        verify(employeeRepository, times(1)).update(employeeToUpdate, employee);
    }

    @Test
    void should_delete_employee_when_delete_given_id() {
        Long id = 1L;

        employeeService.delete(id);

        verify(employeeRepository, times(1)).delete(id);
    }


}
