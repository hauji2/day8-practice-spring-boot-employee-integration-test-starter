package com.afs.restapi;

import java.util.ArrayList;
import java.util.List;

public class FakeEmployeeRepository {

    private final List<Employee> employees = new ArrayList<>();

    public FakeEmployeeRepository() {
        employees.add(new Employee(1L, "John Smith", 32, "Male", 5000));
        employees.add(new Employee(2L, "Jane Johnson", 28, "Female", 6000));
        employees.add(new Employee(3L, "David Williams", 35, "Male", 5500));
        employees.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        employees.add(new Employee(5L, "Michael Jones", 40, "Male", 7000));
    }

    public void reset() {
        employees.clear();
        employees.add(new Employee(1L, "John Smith", 32, "Male", 5000));
        employees.add(new Employee(2L, "Jane Johnson", 28, "Female", 6000));
        employees.add(new Employee(3L, "David Williams", 35, "Male", 5500));
        employees.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        employees.add(new Employee(5L, "Michael Jones", 40, "Male", 7000));
    }

}
