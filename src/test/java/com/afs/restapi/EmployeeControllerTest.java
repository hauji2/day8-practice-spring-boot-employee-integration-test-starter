package com.afs.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @SpyBean
    EmployeeRepository employeeRepository;

    @SpyBean
    EmployeeService employeeService;

    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_getAllEmployees_given_employees() throws Exception {
        mockMvc.perform(get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void should_return_employee_by_id_when_getEmployeesById_given_id() throws Exception {
        String expect = objectMapper.writeValueAsString(
                new Employee(1L, "John Smith", 32, "Male", 5000)
        );

        mockMvc.perform(get("/employees/1"))
                .andExpect(status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_male_employees_when_findEmployeesByGender_given_gender_male() throws Exception {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1L, "John Smith", 32, "Male", 5000));
        employees.add(new Employee(3L, "David Williams", 35, "Male", 5500));
        employees.add(new Employee(5L, "Michael Jones", 40, "Male", 7000));

        String expect = objectMapper.writeValueAsString(employees);

        mockMvc.perform(get("/employees")
                        .param("gender", "Male"))
                .andExpect(status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_employees_by_page_when_findByPage_given_pageNumber_and_pageSize() throws Exception {
        Integer pageNumber = 1;
        Integer pageSize = 2;

        mockMvc.perform(get("/employees")
                        .param("pageNumber", String.valueOf(pageNumber))
                        .param("pageSize", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(result -> jsonPath("$", hasSize(2)));
    }

    @Test
    void should_return_new_employee_when_insertEmployee_given_employee() throws Exception {
        Employee newEmployee = new Employee("hauji2", 32, "Male", 5000);
        System.out.println(objectMapper.writeValueAsString(newEmployee));

        mockMvc.perform(post("/employees")
                        .content(objectMapper.writeValueAsString(newEmployee))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(result -> assertNotNull(result.getResponse().getContentAsString()));

        Employee actual = employeeRepository.findById(6L);
        assertEquals(newEmployee.getAge(), actual.getAge());
        assertEquals(newEmployee.getSalary(), actual.getSalary());
        assertEquals(newEmployee.getGender(), actual.getGender());
        assertEquals(newEmployee.getName(), actual.getName());
        assertTrue(actual.isActiveStatus());
    }

    @Test
    void should_update_employee_when_updateEmployee_given_id_and_employee() throws Exception {
        Employee targetEmployee = new Employee("John Smith", 32, "Male", 9999);

        mockMvc.perform(put("/employees/1")
                        .content(objectMapper.writeValueAsString(targetEmployee))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(result -> assertNotNull(result.getResponse().getContentAsString()))
                .andExpect(result -> {
                    Employee actualEmployee = objectMapper.readValue(result.getResponse().getContentAsString(), Employee.class);
                    assertEquals(targetEmployee.getAge(), actualEmployee.getAge());
                    assertEquals(targetEmployee.getSalary(), actualEmployee.getSalary());
                });
    }

    @Test
    void should_employee_status_false_when_deleteEmployee_given_id() throws Exception {
        mockMvc.perform(delete("/employees/1"))
                .andExpect(status().isNoContent());

        Employee actual = employeeRepository.findById(1L);

        assertFalse(actual.isActiveStatus());
    }


    @Test
    void should_return_404_when_getEmployeesById_given_id_is_not_exist() throws Exception {
        mockMvc.perform(get("/employees/6"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_updateEmployee_given_id_which_does_not_exist() throws Exception {
        Employee targetEmployee = new Employee("John Smith", 32, "Male", 9999);

        mockMvc.perform(put("/employees/66")
                        .content(objectMapper.writeValueAsString(targetEmployee))
                        .contentType("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_deleteEmployee_given_id_which_does_not_exist() throws Exception {
        mockMvc.perform(delete("/employees/66"))
                .andExpect(status().isNotFound());
    }
}
